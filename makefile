COMPILE = ocamlopt
 
default: doubler smart_doubler

doubler: doubler.ml
	$(COMPILE) -o $@ doubler.ml

smart_doubler: smart_doubler.ml
	$(COMPILE) -o $@ smart_doubler.ml

clean :
	rm -f *.cm* *.o doubler smart_doubler
