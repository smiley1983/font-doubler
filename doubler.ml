(*
    Font Doubler
    Copyright (C) 2011  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

*)


let ddebug s = output_string stderr s;

type header =
 {
   version : int;
   flags : int;
   length : int;
   width : int;
   height : int
 }
;;

let new_header v f l w h =
 {
   version = v;
   flags = f;
   length = l;
   width = w;
   height = h; 
 }
;;

let new_character x y = Array.make_matrix y x false;;

type psf2 =
 {
   f_header : header;
   characters : bool array array array
 }
;;

type action = | Header | Begin_char | Next | Nothing;;

let process_headerline head k v =
(*    ddebug "process_headerline\n"; *)
   match k with
    | "Version:" -> head := {!head with version = (int_of_string v)}
    | "Flags:" -> head := {!head with flags = (int_of_string v)}
    | "Length:" -> head := {!head with length = (int_of_string v)}
    | "Width:" -> head := {!head with width = (int_of_string v)}
    | "Height:" -> head := {!head with height = (int_of_string v)}
    | _ -> ()
;;

let process_bitline row c_index h c line =
(*   ddebug "process_bitline\n"; *)
   let col = ref 0 in
   let f ch =
(*
      ddebug ("col = " ^ (string_of_int !col) ^ "\n");
*)
      if ch = '-' then !c.(!c_index).(!row).(!col) <- false
      else if ch = '#' then !c.(!c_index).(!row).(!col) <- true;
      col := !col + 1
   in
   String.iter f line
;;

(*
let chop_leading_spaces s = 
   if String.length s = 0 then ""
   else
     (
      try
         for count = 0 to (String.length s - 1) do
            if String.get s count = ' ' then ()
            else raise (Failure (String.sub s count 
                  (String.length s - count)) )
         done;
         ""
      with Failure r -> r
     )
;;

let chop_trailing_spaces s = 
   if String.length s = 0 then ""
   else
     (
      try
         let l = String.length s in
         for count = 1 to l do
            if String.get s (l - count) = ' ' then ()
            else raise (Failure (String.sub s 0
                  (l - (count - 1))) )
         done;
         ""
      with Failure r -> r
     )
;;
*)

let rec trim s =
   let l = String.length s in 
   if l=0 then s
   else if s.[0]=' ' || s.[0]='\t' || s.[0]='\n' || s.[0]='\r' 
      || s.[0] = '\\'
   then
      trim (String.sub s 1 (l-1))
   else if s.[l-1]=' ' || s.[l-1]='\t' || s.[l-1]='\n' 
      || s.[l-1]='\r' || s.[l-1] = '\\'
   then
      trim (String.sub s 0 (l-1))
   else
      s
;;
let parse_line l =
   let s = trim l in
   let key = ref "" in let v = ref "" in
   if String.contains s ' ' then
     (
      let index = String.index s ' ' in
      key := String.sub s 0 index;
      v := String.sub s (index + 1) (String.length s - (index + 1));
(*
      ddebug ("key, val = " ^ !key ^ ", " ^ !v ^ "\n")
*)
     )
   else
     (
      v := s;
(*
      ddebug ("key, val = " ^ !key ^ ", " ^ !v ^ "\n")
*)
     );
   if !key = "Bitmap:" then 
     (
(*
      ddebug "New bitmap\n";
*)
      !key, !v, Begin_char
     )
   else if String.contains !v '-' || String.contains !v '#' then
      !key, !v, Next
   else match !key with
    | "Version:" | "Flags:"  | "Length:"  | "Width:"  | "Height:" ->
         !key, !v, Header
    | _ -> !key, !v, Nothing
;;

let process_line row c_index h c l not_init =
   let k, v, act = parse_line l in
   match act with
    | Header -> process_headerline h k v
    | Begin_char ->
     (
      if !not_init then
        (
         c := Array.make (!h.length + 1)
               (new_character !h.width !h.height);
         not_init := false
        );
      row := 0;
      c_index := !c_index + 1;
      !c.(!c_index) <- new_character !h.width !h.height;
      if !c_index >= !h.length then 
         output_string stderr "Warning: too many characters";
      process_bitline row c_index h c v
     )
   | Next -> process_bitline row c_index h c v
   | _ -> ()
;;

let read_psf2 (f: string) =
   let in_chan = open_in f in
   let h_head = ref (new_header 0 0 0 0 0) in
   let character = ref (Array.make 1 (new_character 1 1)) in
   let not_eof = ref true in
   let row = ref 0 in
   let c_index = ref (-1) in
   let not_init = ref true in
   let linecount = ref 0 in
   while !not_eof do
     (
      try
         let line = input_line in_chan in
            linecount := !linecount + 1;
(*
            ddebug ("Line " ^ (string_of_int !linecount) ^ "\n");
*)
            process_line row c_index h_head character line not_init;
            row := !row + 1
      with End_of_file -> not_eof := false
     )
   done;
   close_in in_chan;
   {
    f_header = !h_head;
    characters = !character
   }
;;

let write_header h out_chan = 
   output_string out_chan
         ("%PSF2\nVersion: " ^ (string_of_int h.version));
   output_string out_chan ("\nFlags: " ^ (string_of_int h.flags));
   output_string out_chan ("\nLength: " ^ (string_of_int h.length));
   output_string out_chan ("\nWidth: " ^ (string_of_int h.width));
   output_string out_chan
         ("\nHeight: " ^ (string_of_int h.height) ^ "\n");
;;

let output_bitline l out_chan =
   for count = 0 to (Array.length l - 1) do
      let c = if l.(count) then '#' else '-' in
      output_char out_chan c
   done
;;

let output_indent out_chan n =
   let s = String.make n ' ' in
   output_string out_chan s
;;

let write_character c out_chan =
   output_string out_chan "Bitmap: ";
   output_bitline c.(0) out_chan;
   output_string out_chan " \\\n";
   for row = 1 to (Array.length c - 1) do
      output_indent out_chan 8;
      output_bitline c.(row) out_chan;
      if row = Array.length c - 1 then
         output_string out_chan "\n"
      else
         output_string out_chan " \\\n"
   done
;;

let write_characters c out_chan = 
   for count = 0 to (Array.length c - 2) do
      output_string out_chan 
            ("%\n// Character " ^ (string_of_int count) ^ "\n");
      write_character c.(count) out_chan
   done
;;

let write_psf2 ps (f:string) =
   let out_chan = open_out f in
   write_header ps.f_header out_chan;
   write_characters ps.characters out_chan;
   print_string "\nDone!\n";
   close_out out_chan
;;

let test_point c row col =
   try
      c.(row).(col)
   with _ -> false
;;

let double_char c x y =
   let new_c = new_character x y in
   let rowlen = Array.length (c.(0)) in
   for row = 0 to (Array.length c - 1) do
      let bitrow = c.(row) in
      let new_row1 = new_c.(row * 2) in
      let new_row2 = new_c.((row * 2) + 1) in
      for col = 0 to (rowlen - 1) do
         let bit = bitrow.(col) in
(*          if bit then *)
           (
(*
            let above = test_point c (row - 1) col in
            let below = test_point c (row + 1) col in
            let left = test_point c row (col - 1) in
            let right = test_point c row (col + 1) in
            let aboveleft = test_point c (row - 1) (col - 1) in
            let aboveright = test_point c (row - 1) (col + 1) in
            let belowleft = test_point c (row + 1) (col - 1) in
            let belowright = test_point c (row + 1) (col + 1) in
*)
(*
            if (above && below) && (left || not right) then
              (
               new_row1.(col * 2) <- bit;
               new_row2.(col * 2) <- bit;
              );
            if (left && right) && (above || not below) then
              (
               new_row1.(col * 2) <- bit;
               new_row1.((col * 2) + 1) <- bit;
              );
            if above && below && right then
              (
               new_row1.((col * 2) + 1) <- bit;
               new_row2.((col * 2) + 1) <- bit;
              );
            if left && right && below then
              (
               new_row2.(col * 2) <- bit;
               new_row2.((col * 2) + 1) <- bit;
              );
            if above && left then new_row1.(col * 2) <- bit;
            if above && right then new_row1.(col * 2 + 1) <- bit;
            if below && left then new_row2.(col * 2) <- bit;
            if below && right then new_row2.(col * 2 + 1) <- bit;
            if aboveleft then
               new_row1.(col * 2) <- bit;
            if aboveright then
               new_row1.((col * 2) + 1) <- bit;
            if belowleft then
               new_row2.(col * 2) <- bit;
            if belowright then
               new_row2.((col * 2) + 1) <- bit;
            if above && not (left || right || below) then
               new_row1.(col * 2) <- bit;
            if below && not (left || right || above) then
               new_row2.(col * 2) <- bit;
            if left && not (above || below || right) then
               new_row1.(col * 2) <- bit;
            if right && not (above || below || left) then
               new_row1.((col * 2) + 1) <- bit;
            if not (left || right || above || below) then
*)
              (
                 (
                  new_row1.((col * 2) + 1) <- bit;
                  new_row1.(col * 2) <- bit;
                  new_row2.(col * 2) <- bit;
                  new_row2.((col * 2) + 1) <- bit;
                 )
              )
           )
      done;
(*
      new_c.(row * 2) <- new_row1;
      new_c.((row * 2) + 1) <- new_row2;
*)
   done;
   new_c
;;

let double_chars c x y =
   let new_c = Array.make (Array.length c) (new_character x y) in
   for count = 0 to (Array.length c - 1) do
      new_c.(count) <- double_char c.(count) x y
   done;
   new_c
;;

let double_psf p =
   let header = 
     {p.f_header with width = (p.f_header.width * 2);
      height = (p.f_header.height * 2)}
   in
   let chars = double_chars p.characters header.width header.height in
     {
      f_header = header;
      characters = chars
     }
;;

let main () = 
   let f = Sys.argv.(1) in
   let psf = read_psf2 f in
   let doubled = double_psf psf in
      write_psf2 doubled ("double_" ^ f)
;;

main () ;;
